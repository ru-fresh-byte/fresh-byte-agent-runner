#if defined(OS400)
#pragma convert(850)
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <global.h>
#include <stdnames.h>
#include <odstypes.h>       
#include <queryods.h>      
#include <editods.h>
#include <nsfdb.h>
#include <nsfdata.h>
#include <nsfnote.h>
#include <nif.h>
#include <nsfsearc.h>       
#include <nsfobjec.h>       
#include <kfm.h>            
#include <osmem.h>
#include <ostime.h>
#include <agents.h>
#include <osmisc.h> 
#include <osfile.h>
#include <lapiplat.h>
#include <getopt.h>

#if !defined(ND64) 
#define DHANDLE HANDLE 
#endif

/* Function Prototypes */
STATUS  LNPUBLIC  RunTriggeredAgents(DBHANDLE, char*, NOTEHANDLE);
STATUS  LNPUBLIC  GetAgentRunInfo(DBHANDLE, char*);
STATUS  LNPUBLIC  OpenAgent(DBHANDLE, char*, HAGENT*);
STATUS  LNPUBLIC  SetRunContext(HAGENT, HAGENTCTX*, NOTEHANDLE);
STATUS  LNPUBLIC  ExecuteAgent(HAGENT, HAGENTCTX);
void    LNPUBLIC  GetScriptRunInfo(HAGENTCTX);
void    LNPUBLIC  CloseAgentContext(HAGENTCTX);
void    LNPUBLIC  CloseAgent(HAGENT);
void PrintAPIError(STATUS);


FILE* pAgentsLog;

static struct option long_options[] = {
    {"log", 1, 0, 'l'},
    {"userid", 1, 0, 'u'},
    {"password", 1, 0, 'p'},
    {"notes", 1, 0, 'n'},
    {"server", 1, 0, 's'},
    {"database", 1, 0, 'd'},
    {"agent", 1, 0, 'a'},
    {"context", 1, 0, 'c'},
    {"help", 1, 0, 'h'},
    {0, 0, 0, 0}
};

int defineParameters(
    int argc, 
    char* argv[],
    char* userid_path[],
    char* userid_password[],
    char* notes_path[],
    char* server_name[],
    char* db_path[],
    char* agent_name[],
    char* note_id[],
    char* log_path[]
) {
    int result = 0;

    int option_index = 0;
    int c;
    while ((c = getopt_long(argc, argv, "u:p:n:s:d:a:c:l:h:", long_options, &option_index)) != -1) {

        switch (c) {
        case 'u':
            *userid_path = optarg;
            break;
        case 'p':
            *userid_password = optarg;
            break;
        case 'n':
            *notes_path = optarg;
            break;
        case 's':
            *server_name = optarg;
            break;
        case 'd':
            *db_path = optarg;
            break;
        case 'a':
            *agent_name = optarg;
            break;
        case 'c':
            *note_id = optarg;
            break;
        case 'l':
            *log_path = optarg;
            break;
        case 'h':
            result = 1;
            break;
        case '?':
            printf("Unknown option character: %s\n", optarg);
            return 1;
        default:
            printf("\nArgument:  %d", c);
            abort();
        }
    }

    
    if (*notes_path == "") {
        printf("\nPath of Notes Directory is not defined");
        result = 1;
    }

    if (*db_path == "") {
        printf("\nPath of Domino database is not defined");
        result = 1;
    }

    if (*note_id != "") {
        int length = strlen(*note_id);
        if (length != 6) {
            printf("\nNoteId must contains 6 symbols %d %s", length, *note_id);
            result = 1;
        }
    }

    if (*log_path == "") {
        printf("\nPath of log file is not defined");
        result = 1;
    }

    if (result == 1) {
        printf("\n\nUsage:  %s: \n-n --notes      - directory of notes;\n[-u --userid]   - path to UserID file;\n[-p --password] - password to UserID file;\n[-s --server]   - server name;\n-d --database   - database file path;\n-a --agent      - agent name;\n-c --context    - note id of context document (6 symblols);\n-l --log        - path to log file;\n[-h --help]\n", argv[0]);
    }

    return result;
}


int main(int argc, char* argv[])
{
    STATUS error = NOERROR;

    char* notes_path[] = { "" };          /* path to notes directory*/
    char* userid_path[] = { "" };         /* path of UserId file*/
    char* userid_password[] = { "" };     /* password of UserId file*/
    char* server_name[] = { "" };         /* server name where database lives*/
    char* db_path[] = { "" };             /* path of database */
    char* agent_name[] = { "" };          /* name of agent */
    char* note_id[] = { "" };             /* noteid of context */
    char* log_path[] = { "agents.log" };  /* path of log file*/

    int isWrongArguments = defineParameters(argc, argv, userid_path, userid_password, notes_path,server_name, db_path, agent_name, note_id, log_path);
    if (isWrongArguments == 1) {
        return (0);
    }

    DBHANDLE    hDb;
    char* arr[255] = {*notes_path};

    if (error = NotesInitExtended(1, arr))
    {
        printf("\n Unable to initialize Notes.");
        return (1);
    }

    char UserName[255];
    if (strcmp(*userid_path, "")) {
        SECKFMSwitchToIDFile(*userid_path, *userid_password, UserName, 30, fKFM_switchid_DontSetEnvVar, NULL);
    } else {
        SECKFMGetUserName(UserName);
    }
    printf("\nAgent will run with rights of user: %s", UserName);

    if (strcmp(*server_name, "")) {
        char pname[MAXPATH] = "";
        error = OSPathNetConstruct(NULL, *server_name, *db_path, pname);
        if (error) {
            PrintAPIError(error);
            goto Exit0;
        }
        *db_path = pname;
    }   
    printf("\nDatabase: %s\n", *db_path);

    /* Open input database */
    if (error = NSFDbOpen(*db_path, &hDb))
    {
        printf("Error: unable to open target database '%s'\n", *db_path);
        goto Exit0;
    }

    /* Open file pointer to output log */
    if ((pAgentsLog = fopen(*log_path, "w+")) == NULL)
    {
        printf("Error: unable to open output log file '%s'\n", *log_path);
        goto Exit0;
    }

    NOTEHANDLE hNote = NULLHANDLE;
    if (strcmp(*note_id, ""))
    {
        char* endptr = *note_id + 5;
        NOTEID noteId = strtoul(*note_id, &endptr, 16);
        if (error = NSFNoteOpen(hDb, noteId, 0, &hNote))
        {
            PrintAPIError(error);
            goto Exit0;
        }
    }

    /* Now force execution of the two trigerred agent notes */
    if (error = RunTriggeredAgents(hDb, *agent_name, hNote)) {
        goto Exit1;
    }   

    fprintf(pAgentsLog, "Successfully executed the agent '%s' in '%s'.\n", *agent_name, *db_path);
    printf("Program execution completed.  See logfile '%s' for results.\n", *log_path);
    fprintf(pAgentsLog, "Program execution completed.\n");

    
    /* Close database, close log file pointer. */
Exit1:
    NSFDbClose(hDb);
    fclose(pAgentsLog);

Exit0:
    if (error)
        PrintAPIError(error);

    NotesTerm();
    return(error);
}

STATUS  LNPUBLIC  RunTriggeredAgents(
    DBHANDLE hDb, 
    char* agentName,
    NOTEHANDLE  hParmNote
) {
    STATUS      error = NOERROR;
    HAGENT      hAgent;
    HAGENTCTX   hAgentRunCtx;
    
    if (error = OpenAgent(hDb, agentName, &hAgent))
    {
        goto Exit0;
    }

    if (error = SetRunContext(hAgent, &hAgentRunCtx, hParmNote))
    {
        goto Exit1;
    }

    if (error = ExecuteAgent(hAgent, hAgentRunCtx))
    {
        goto Exit1;
    }

    fprintf(pAgentsLog, "Successfully ran agent '%s'.\n", agentName);

    GetScriptRunInfo(hAgentRunCtx);

    fprintf(pAgentsLog, "Successfully read run data for agent '%s'.\n\n", agentName);

Exit1:
    CloseAgentContext(hAgentRunCtx);
    CloseAgent(hAgent);

Exit0:
    return(error);
}

STATUS  LNPUBLIC  OpenAgent(
    DBHANDLE hDb, 
    char* szAgentName,
    HAGENT* hOpenAgent
) {
    STATUS          error = NOERROR;

    /* Execute Agent Variables */
    NOTEID          AgentId;

    /* Given the handle of the database and the Agent name,
     * find the Note ID for the Agent.
     */
    if (error = NIFFindDesignNote(hDb, szAgentName,
        NOTE_CLASS_FILTER, &AgentId))
    {
        error = NIFFindPrivateDesignNote(hDb, szAgentName,
            NOTE_CLASS_FILTER, &AgentId);
        if (error)
            return (error);
    }

    /* Now open up, and return a handle to the open agent */
    error = AgentOpen(hDb, AgentId, hOpenAgent);
    return (error);
}


STATUS  LNPUBLIC  SetRunContext(
    HAGENT hOpenAgent,
    HAGENTCTX* hOpenAgentCtx,
    NOTEHANDLE  hParmNote
) {
    STATUS          error = NOERROR;

    if (error = AgentCreateRunContext(hOpenAgent, NULL, (DWORD)0, hOpenAgentCtx))
    {
        goto Exit0;
    }
    
    if (error = AgentRedirectStdout(*hOpenAgentCtx, AGENT_REDIR_MEMORY)) 
    {
        goto Exit0;
    }
    
    if (hParmNote != NULLHANDLE)
    {
        error = AgentSetDocumentContext(*hOpenAgentCtx, hParmNote);
    }

Exit0:
    return (error);
}


STATUS  LNPUBLIC  ExecuteAgent(
    HAGENT hOpenAgent, 
    HAGENTCTX hOpenAgentCtx
) {
    STATUS          error = NOERROR;

    error = AgentRun(hOpenAgent, hOpenAgentCtx, (DHANDLE)0, (DWORD)0);
    return (error);
}


void  LNPUBLIC  GetScriptRunInfo(HAGENTCTX hOpenAgentCtx)
{
    DHANDLE           hStdout;
    DWORD           dwLen;
    char* pStdout;

    AgentQueryStdoutBuffer(hOpenAgentCtx, &hStdout, &dwLen);
    pStdout = OSLock(char, hStdout);
    pStdout[dwLen] = '\0';
    fprintf(pAgentsLog, "\nRedirected PRINT statements:\n%s\n", pStdout);
    OSUnlock(hStdout);

    return;
}


void  LNPUBLIC  CloseAgent(HAGENT hOpenAgent)
{

    /* Close open agent handle */
    AgentClose(hOpenAgent);

    return;
}


void  LNPUBLIC  CloseAgentContext(HAGENTCTX hOpenAgentCtx)
{

    /* If specified, close the open agent context handle */
    if (hOpenAgentCtx != NULLHANDLE)
        AgentDestroyRunContext(hOpenAgentCtx);

    return;
}


STATUS  LNPUBLIC  GetAgentRunInfo(
    DBHANDLE hDb, 
    char* szAgentName
) {
    STATUS          error = NOERROR;
    NOTEID                      AgentId;
    NOTEHANDLE      hAgentNote;

    /* $AssistRunInfo run data object variables */
    OBJECT_DESCRIPTOR           objRunInfo;
    BLOCKID                     bidRunInfo;
    WORD                        wDataType;
    DWORD                       dwItemSize;
    char* pObject;
    DWORD                       dwObjectSize;
    WORD                        wClass;
    WORD                        wPrivs;
    WORD                        wCounter;
    DHANDLE                       hBuffer;
    DWORD                       dwOffset;
    ODS_ASSISTRUNOBJECTHEADER   RunHeader;
    ODS_ASSISTRUNOBJECTENTRY    RunEntry[5];
    ODS_ASSISTRUNINFO           RunInfo;
    char                        RunTime[MAXALPHATIMEDATE + 1];
    WORD                        wLength;

    /* Given the handle of the database and the Agent name,
     * find the Note ID for the Agent.
     */
    if (error = NIFFindDesignNote(hDb, szAgentName,
        NOTE_CLASS_FILTER, &AgentId))
    {
        if (error = NIFFindPrivateDesignNote(hDb, szAgentName, NOTE_CLASS_FILTER, &AgentId))
            goto Exit0;
    }

    /* Now report the results of the agent execution stored in the
       $AssistRunInfo object item of the Agent note */
    if (error = NSFNoteOpen(hDb, AgentId, (WORD)0, &hAgentNote))
        goto Exit0;

    if (error = NSFItemInfo(hAgentNote, ASSIST_RUNINFO_ITEM,
        (WORD)strlen(ASSIST_RUNINFO_ITEM),
        NULL, &wDataType, &bidRunInfo, &dwItemSize))
        goto Exit1;

    /* assign pointer to item, and skip over TYPE_OBJECT word */
    pObject = OSLockBlock(char, bidRunInfo);
    pObject += ODSLength(_WORD);

    /* Read in the OBJECT_DESCRIPTOR ODS record and ensure that there are
    data objects */
    ODSReadMemory(&pObject, _OBJECT_DESCRIPTOR, &objRunInfo, 1);
    OSUnlockBlock(bidRunInfo);
    if (error = NSFDbGetObjectSize(hDb, objRunInfo.RRV, objRunInfo.ObjectType,
        &dwObjectSize, &wClass, &wPrivs))
        goto Exit1;

    if (dwObjectSize == 0)
    {
        fprintf(pAgentsLog, "Invalid run data object item\n\n");
        goto Exit1;
    }

    /*
     * Sequentially read in the header, entries, and related run information objects
     * that are created during an agent execution.
     */
    dwOffset = 0;

    /*
     * ODS_ASSISTRUNOBJECTHEADER - contains the number of run data object entries.
     * By default, there are at least 5 objects reserved for run data.  Of
     * importance, the first of these always contains the RunInfo data.   The
     * third of these always contains an agent execution log.
     */
    if (error = NSFDbReadObject(hDb, objRunInfo.RRV, dwOffset,
        ODSLength(_ODS_ASSISTRUNOBJECTHEADER), &hBuffer))
        goto Exit1;
    pObject = OSLock(char, hBuffer);
    ODSReadMemory(&pObject, _ODS_ASSISTRUNOBJECTHEADER, &RunHeader, 1);
    OSUnlock(hBuffer);
    OSMemFree(hBuffer);
    if (RunHeader.wEntries == 0)
    {
        fprintf(pAgentsLog, "No run data entries to report; Agent never executed\n\n");
        goto Exit1;
    }
    dwOffset += ODSLength(_ODS_ASSISTRUNOBJECTHEADER);

    /*
     * ODS_ASSISTRUNOBJECTENTRY - determines which entry has run data.
     * If the entry length of both the run info data and log objects are greater
     * than zero, than the agent has been executed at least once and run data
     * exists.  */

    if (error = NSFDbReadObject(hDb, objRunInfo.RRV, dwOffset,
        ODSLength(_ODS_ASSISTRUNOBJECTENTRY) * RunHeader.wEntries, &hBuffer))
        goto Exit1;
    pObject = OSLock(char, hBuffer);

    /* read the first entry and make sure that a RunInfo data object exists. */
    ODSReadMemory(&pObject, _ODS_ASSISTRUNOBJECTENTRY, &RunEntry[0], 1);
    if (RunEntry[0].dwLength == 0)
    {
        fprintf(pAgentsLog, "No run information data to report; Agent never executed\n\n");
        goto Exit1;
    }
    fprintf(pAgentsLog, "Agent Run Information:\n");

    /* read the third entry and make sure that an execution log object exists.
     * Note - we check this only because the Notes UI creates an empty RunInfo
     * data object when an agent is executed, but only creates the log object
     * if executed.
     */
    ODSReadMemory(&pObject, _ODS_ASSISTRUNOBJECTENTRY, &RunEntry[1], 1);
    ODSReadMemory(&pObject, _ODS_ASSISTRUNOBJECTENTRY, &RunEntry[2], 1);
    if (RunEntry[2].dwLength == 0)
    {
        fprintf(pAgentsLog, "Agent log does not exist; Never executed\n\n");
        goto Exit1;
    }

    /* and skip over the remaining reserved run data objects */
    for (wCounter = 3; wCounter < RunHeader.wEntries; wCounter++)
        ODSReadMemory(&pObject, _ODS_ASSISTRUNOBJECTENTRY, &RunEntry[wCounter], 1);

    dwOffset += ODSLength(_ODS_ASSISTRUNOBJECTENTRY) * RunHeader.wEntries;
    OSUnlockObject(hBuffer);
    OSMemFree(hBuffer);

    /*
     * ODS_ASSISTRUNINFO - contain the Run Information.
     * It is always the first run data object.
     */
    if (error = NSFDbReadObject(hDb, objRunInfo.RRV, dwOffset,
        ODSLength(_ODS_ASSISTRUNINFO), &hBuffer))
        goto Exit1;
    pObject = OSLock(char, hBuffer);
    ODSReadMemory(&pObject, _ODS_ASSISTRUNINFO, &RunInfo, 1);
    OSUnlock(hBuffer);
    OSMemFree(hBuffer);
    (void)ConvertTIMEDATEToText(NULL, NULL, &RunInfo.LastRun, RunTime,
        MAXALPHATIMEDATE, &wLength);
    RunTime[wLength] = '\0';
    fprintf(pAgentsLog, "TimeDate of Execution: %s\n", RunTime);
    fprintf(pAgentsLog, "Documents Processed By Formula: %ld\n", RunInfo.dwProcessed);
    fprintf(pAgentsLog, "Exit Code: %ld\n\n", RunInfo.dwExitCode);

    /* skip the next one, display the execution log, and dump the rest */
    dwOffset += RunEntry[0].dwLength;
    for (wCounter = 1; wCounter < RunHeader.wEntries; wCounter++)
    {
        if (RunEntry[wCounter].dwLength != 0)
        {
            if (error = NSFDbReadObject(hDb, objRunInfo.RRV, dwOffset,
                RunEntry[wCounter].dwLength, &hBuffer))
                goto Exit1;
            pObject = OSLock(char, hBuffer);
            if (wCounter == 2)
            {
                pObject[RunEntry[wCounter].dwLength] = '\0';
                fprintf(pAgentsLog, "Agent Log <Start>:\n%s", pObject);
                fprintf(pAgentsLog, "Agent Log <End>:\n");
            }
            dwOffset += RunEntry[wCounter].dwLength;
            OSUnlock(hBuffer);
            OSMemFree(hBuffer);
        }
    }

Exit1:
    NSFNoteClose(hAgentNote);

Exit0:
    return (error);
}


void PrintAPIError(STATUS api_error)
{
    STATUS  string_id = ERR(api_error);
    char    error_text[200];
    WORD    text_len;

    /* Get the message for this IBM C API for Notes/Domino error code
       from the resource string table. */

    text_len = OSLoadString(NULLHANDLE,
        string_id,
        error_text,
        sizeof(error_text));

    /* Print it. */
    fprintf(stderr, "\n%s\n", error_text);

}

